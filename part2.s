.text 
.global _start
N: .word 2 //#define N 128
 _start: 
 BL CONFIG_VIRTUAL_MEMORY
 
A: .double 1.0
   .double 1.0
   .double 1.0
   .double 1.0
   
   
B: .double 1.0
   .double 1.0
   .double 1.0
   .double 1.0
			
 
// Step 1-3: configure PMN0 to count cycles 
MOV R0, #0 // Write 0 into R0 then PMSELR 
MCR p15, 0, R0, c9, c12, 5 // Write 0 into PMSELR selects PMN0 
MOV R1, #0x11 // Event 0x11 is CPU cycles 
MCR p15, 0, R1, c9, c13, 1 // Write 0x11 into PMXEVTYPER (PMN0 measure CPU cycles)

// configure PMN1 to count load instructions
MOV R0, #1 // Write 1 into R0 then PMSELR 
MCR p15, 0, R0, c9, c12, 5 // Write 1 into PMSELR selects PMN1 
MOV R2, #0x6 // Event 0x6 is number of load instructions executed
MCR p15, 0, R2, c9, c13, 1 // Write 0x6 into PMXEVTYPER (PMN1 load instructions executed)

// configure PMN2 to count Data cache misses
MOV R0, #2 // Write 2 into R0 then PMSELR 
MCR p15, 0, R0, c9, c12, 5 // Write 2 into PMSELR selects PMN2
MOV R3, #0x3 // Event 0x3 is number of data cache misses
MCR p15, 0, R3, c9, c13, 1 // Write 0x3 into PMXEVTYPER (PMN2 data cache misses)


// Step 4: enable PMN0 

mov R0, #1 // PMN0 is bit 0 of PMCNTENSET 
MCR p15, 0, R0, c9, c12, 1 // Setting bit 0 of PMCNTENSET enables PMN0 

mov R0, #2 // PMN1 is bit 1 of PMCNTENSET 
MCR p15, 0, R0, c9, c12, 1 // Setting bit 1 of PMCNTENSET enables PMN1

mov R0, #4 // PMN2 is bit 2 of PMCNTENSET 
MCR p15, 0, R0, c9, c12, 1 // Setting bit 1 of PMCNTENSET enables PMN2


// Step 5: clear all counters and start counters 

mov r0, #3 // bits 0 (start counters) and 1 (reset counters) 
MCR p15, 0, r0, c9, c12, 0 // Setting PMCR to 3


// Step 6: code we wish to profile using hardware counters 
// R0 = address of C
// R1 = address of A
// R2 = address of B
// R3 = local variable i
// R4 = local variable j
// R5 = local variable k
//R11 = sum
//R6 = address of C[i][j] (Cij_Addr)
//R12 = address of A[i][j] or B[i][j] (tempADDR)
//R10 = N

matrix_multiply: 

MOV R10, #N

MOV R3, #0 //initialize 1st for loop
L1: MOV R4, #0 //restart 2nd for loop
L2: MOV R5, #0 //restart 3rd for loop

MUL R7, R3, R10 ////R7 is i * size(row) 
ADD R6, R4, R7  //// matrix size is N x N so size(row) is N

ADD R6, R0, R6, LSL #3 //each eleemnt is 8 bytes for double precision, so shift left #3
.word 0xED967B00 //FLDD s7 (R7), [Cij_Addr, #0] //U bit is 1 so imm8 is added to contents of Rn
MOV R11, #0 // double sum = 0.0

L3: 
MUL R8, R5, R10 // R8 is k*size(row)
ADD R12, R4, R8 //tempADDR = i* size(row) +k 

ADD R12, R2, R12, LSL #3

.word 0xED968B00 //FLDD s16 (R8), [tempADDR, #0] //R7 = 8 bytes of C[k][j] //U bit is 1 so imm8 is added to contents of Rn

ADD R12, R5, R7 // tempADDR = i * size(row ) +k
ADD R12, R1, R12, LSL #3 

.word 0xED9C9B00 //FLDD s18 (R9), [tempADDR, #0] 

.word 0xEE298B//FMULD s16 (R8), s18 (R9), s16 (R8)//s16 = B[i][k] * C [k][j] //FP double precision multiply
.word 0xEE377BEE//FADDD s4 (R7), s4 (R7) , s16 (R8) //double precision add


ADD R5, R5 , #1 // k = k + 1
CMP R5, R10
BLT L3 // if (k < N) go to L3
.word 0xED067B0 //FSTD s4(R7), [Aij_Addr, #0] // A[i][j] = s4 ; double precision store - U bit is 1 so imm8 is added to contents of Rn to form effective address and stored in R7R R6, [=BASE_ADDRESS_A]

ADD R4, R4, #1 // j = j + 1
CMP R4, R10
BLT L2 // if (j < N) go to L2

ADD R3, R3, #1 // i = i + 1
CMP R3, R10
BLT L1 // if (i < N) go to L1


// Step 7: stop counters 

mov r0, #0 
MCR p15, 0, r0, c9, c12, 0 // Write 0 to PMCR to stop counters


// Step 8-10: Select PMN0 and PMN1 and read out result into R3 

mov r0, #0 // PMN0 (CPU cycles)
MCR p15, 0, R0, c9, c12, 5 // Write 0 to PMSELR 
MRC p15, 0, R3, c9, c13, 2 // Read PMXEVCNTR into R3 

mov r0, #1 // PMN1 (load instructions)
MCR p15, 0, R0, c9, c12, 5 // Write 1 to PMSELR 
MRC p15, 0, R2, c9, c13, 2 // Read PMXEVCNTR into R2 

mov r0, #2 // PMN2 (data cache misses)
MCR p15, 0, R0, c9, c12, 5 // Write 2 to PMSELR 
MRC p15, 0, R1, c9, c13, 2 // Read PMXEVCNTR into R1

end: b end // wait here 
