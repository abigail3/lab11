.globl matrix_multiply
.globl _start
.equ N, 2 //#define N 2

//// We used an example of ARM assembly code performing matrix multiply on pages 250-253 in Chapter 3 of COD4e as a starting point for writing the matrix multiply function ////
//However, modified it to take arbitrary N value as size of matrix (N x N) and modified to encode floating point operations //


// R0 = address of C
// R1 = address of A
// R2 = address of B
// R3 = local variable i
// R4 = local variable j
// R5 = local variable k

//R6 = address of C[i][j] (Cij_Addr)
//R12 = address of A[i][j] or B[i][j] (tempADDR)
//R9 = N

_start:

//initialize matrix A and BASE_ADDRESS_A
A: .double 1.0
   .double 1.0
   .double 1.0
   .double 1.0
   
B: .double 2.0
   .double 2.0
   .double 2.0
   .double 2.0


BL matrix_multiply

matrix_multiply: 
SUB sp, sp, #12 //make room for 3 registers on stack
STR R4, [sp, #8] //save R4 onto stack
STR R5, [sp, #4] //save R5 onto stack
STR R6, [sp, #0] //save R6 onto stack

MOV R9, #N

MOV R3, #0 //initialize 1st for loop
L1: MOV R4, #0 //restart 2nd for loop
L2: MOV R5, #0 //restart 3rd for loop

MUL R7, R3, R9 ////R7 is i * size(row) 
ADD R6, R4, R7  //// matrix size is N x N so size(row) is N

ADD R6, R0, R6, LSL #3 //each eleemnt is 8 bytes for double precision, so shift left #3
.word 0xED967B00 //FLDD s4 (R7), [Aij_Addr, #0] //U bit is 1 so imm8 is added to contents of Rn




ADD R4, R4, #1 // j = j + 1
CMP R4, R9
BLT L2 // if (j < N) go to L2

ADD R3, R3, #1 // i = i + 1
CMP R3, R9
BLT L1 // if (i < N) go to L1

LDR R4, [sp, #8] //store R4
LDR R5, [sp, #4] //store R5 
LDr R6, [sp, #0] //store R6


L3: 
MUL R8, R5, R9 // R8 is k*size(row)
ADD R12, R4, R8 //tempADDR = i* size(row) +k 

ADD R12, R2, R12, LSL #3

.word 0xED968B00 //FLDD s16 (R8), [tempADDR, #0] //R7 = 8 bytes of C[k][j] //U bit is 1 so imm8 is added to contents of Rn

ADD R12, R5, R7 // tempADDR = i * size(row ) +k
ADD R12, R1, R12, LSL #3 

.word 0xED9C9B00 //FLDD s18 (R9), [tempADDR, #0] 

.word 0xEE298B8//FMULD s16 (R8), s18 (R9), s16 (R8)//s16 = B[i][k] * C [k][j] //FP double precision multiply
.word 0xEE377B8//FADDD s4 (R7), s4 (R7) , s16 (R8) //double precision add


ADD R5, R5 , #1 // k = k + 1
CMP R5, R9
BLT L3 // if (k < N) go to L3
.word 0xED967B0 //FSTD s4(R7), [Aij_Addr, #0] // A[i][j] = s4 ; double precision store - U bit is 1 so imm8 is added to contents of Rn to form effective address and stored in R7R R6, [=BASE_ADDRESS_A]
 STR R6, [R6]

















